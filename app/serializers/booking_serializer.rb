class BookingSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :end_time, :length
  has_one :meeting_room
  has_one :user
end