class CompactCountrySerializer < ActiveModel::Serializer
  attributes :name, :slug
end