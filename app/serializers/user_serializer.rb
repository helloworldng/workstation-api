class UserSerializer < ActiveModel::Serializer
  attributes :uid, :full_name, :email, :organization_id
  has_one :home_location
  has_one :plan

end