class ReceiptSerializer < ActiveModel::Serializer
  attributes :id, :notification_id, :is_read, :trashed, :deleted, :mailbox_type, :created_at, :updated_at
  has_one :message
end