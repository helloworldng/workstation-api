class PlanSerializer < ActiveModel::Serializer
  attributes :name, :description, :monthly_cost, :tokens, :slug
end