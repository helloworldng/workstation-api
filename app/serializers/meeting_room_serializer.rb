class MeetingRoomSerializer < ActiveModel::Serializer
  attributes :id, :name
end