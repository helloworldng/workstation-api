class OrganizationInviteSerializer < ActiveModel::Serializer
  attributes :id, :approved
  has_one :sender
  has_one :recipient
end