class OrganizationSerializer < ActiveModel::Serializer
  attributes :name
  has_one :admin
  has_many :users
end