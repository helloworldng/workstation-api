class AdminDashboard::V1::CountriesController < AdminDashboardController


  def index
    @countries = Country.all.collect { |country| {name: country.data["name"], code: country.data["country_code"]} }
    render json: @countries, status: 201
  end

  def show
    verify_id_params
    set_country
    create_country_hash
    render json: @country_hash, status: 200
  end

  private

  def set_country
    @country = Country.find_country_by_country_code(params[:id])
  end

  def verify_id_params
    param! :id, Integer, required: true
  end


  def create_country_hash
    @country_hash = {
        name: @country.data["name"],
        code: @country.data["country_code"],
        states: @country.states.collect { |state|
          {
              name: state[1]["name"], shortcode: state[0]
          }
        }
    }
  end

end