class AdminDashboard::V1::LocationsController < AdminDashboardController
  before_action :authenticate_admin!, except: [:index, :show]

  def index
    @locations = Location.all
    paginate json: @locations
  end

  def create
    validate_create_params
    @location = Location.create(location_params)
    render json: @location, status: 201
  end

  def show
    param! :id, Integer, required: true
    render json: Location.find(params[:id]), status: 200
  end

  def update
    validate_update_params
    set_location
    if @location.update(location_params)
      render @location, status: 201
    else
      render json: {errors: @location.full_messages}, status: 422
    end
  end

  def destroy
    param! :id, Integer, required: true
    @location = Location.find(params[:id])
    @location.destroy
    head status: 204
  end

  def admins
    param! :id, Integer, require: true
    set_location
    @admins = @location.users
    render json: @users, status: 200
  end

  def users
    param! :id, Integer, required: true
    set_location
    @users = @location.users
    render json: @users, status: 200
  end

  private

  def set_location
    @location = Location.find(params[:id])
  end

  def validate_create_params
    param! :street_address, String, required: true
    param! :country_code, Integer, required: true
    param! :state_shortcode, String, required: true
    param! :description, String, required: true
  end

  def validate_update_params
    param! :id, Integer, required: true
    param! :street_address, String
    param! :country_code, Integer
    param! :state_shortcode, String
    param! :description, String
  end


  def location_params
    params.permit(:street_address, :country_code, :state_shortcode, :description)
  end

end