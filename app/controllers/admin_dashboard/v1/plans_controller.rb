class AdminDashboard::V1::PlansController < AdminDashboardController
  before_action :authenticate_admin!, except: [:index, :show]


  def index
    @plans = Plan.all
    paginate json: @plans, status: 200
  end

  def show
    set_plan
    render json: @plan, status: 200
  end

  def create
    verify_create_params
    @plan = Plan.create(plan_params)
    render json: @plan, status: 201
  end

  def update
    verify_update_params
    set_plan
    @plan.update(plan_params)
    render json: @plan, status: 201
  end

  def destroy
    @plan.destroy
    head status: 204
  end

  def subscribers
    set_plan
    @subscribers = @plan.users
    paginate json: @subscribers, status: 200
  end



  private


  def plan_params
    params.permit(:name, :description, :monthly_price, :yearly_price, :daily_price, :tokens)
  end

  def set_plan
    param! :id, String, required: true
    @plan = Plan.find.friendly(params[:id])
  end


  def verify_create_params
    param! :name, String, required: true
    param! :description, String, required: true
    param! :daily_price, BigDecimal, required: true
    param! :monthly_price, BigDecimal, required: true
    param! :yearly_price, BigDecimal, required: true
    param! :tokens, Integer, required: true
  end

  def verify_update_params
    param! :name, String
    param! :description, String
    param! :daily_price, BigDecimal
    param! :monthly_price, BigDecimal
    param! :yearly_price, BigDecimal
    param! :tokens, Integer
  end

end