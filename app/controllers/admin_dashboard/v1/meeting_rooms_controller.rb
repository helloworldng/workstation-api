class AdminDashboard::V1::MeetingRoomsController < ApplicationController
  before_action :authenticate_admin!

  def index
    @meeting_rooms = MeetingRoom.all
    paginate json: @meeting_rooms, status: 200
  end

  def create
    validate_create_params
    @meeting_room = MeetingRoom.create(meeting_room_params)
    if @meeting_room.save
      render @meeting_room, status: 201
    else
      render json: {errors: @meeting_room.full_messages}, status: 422
    end
  end

  def destroy
    set_meeting_room
    @meeting_room.destroy
    head status: 204
  end

  def update
    validate_create_params
    set_meeting_room
    if @meeting_room.update meeting_room_params
      render @meeting_room, status: 201
    else
      render json: {errors: @meeting_room.full_messages}, status: 422
    end
  end

  private

  def validate_create_params
    param! :name, String, required: true
    param! :location_id, String, required: true
  end

  def meeting_room_params
    params.require(:meeting_room).permit(:name, :location_id, :delete)
  end

  def set_meeting_room
    param :id, Integer, required: true
    @meeting_room = MeetingRoom.find(params[:id])
  end

end
