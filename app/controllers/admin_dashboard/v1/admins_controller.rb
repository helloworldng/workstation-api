class AdminDashboard::V1::AdminsController < AdminDashboardController
  before_action :check_superadmin, only: [:admins]


  def admins
    if params[:location_id]
      @admins = Admin.where(home_location_id: params[:location])
    else
      @admins = Admin.all
    end
    paginate json: @admins, status: 200
  end


  private

  def check_superadmin
    if !current_admin.superadmin
      render json: {errors: "You're not allowed abeg. Just free!"}, status: 400
    end
  end

end