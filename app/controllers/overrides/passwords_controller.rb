module Overrides
  class PasswordsController < DeviseTokenAuth::PasswordsController

    def is_json_api
      return false
    end

  end
end