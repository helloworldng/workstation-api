module Overrides
  class SessionsController < DeviseTokenAuth::SessionsController

    def is_json_api
      return false
    end

  end
end