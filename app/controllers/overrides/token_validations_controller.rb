module Overrides
  class TokenValidationsController < DeviseTokenAuth::TokenValidationsController

    def is_json_api
      return false
    end

  end
end