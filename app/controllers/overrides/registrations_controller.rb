module Overrides
  class RegistrationsController < DeviseTokenAuth::RegistrationsController
    after_action :add_to_organization

    def is_json_api
      return false
    end

    private


    def add_to_organization
      param! :invite_token, String
      if params[:invite_token]
        ActiveRecord::Base.transaction do
          @invite = OrganizationInvite.find_by_token(params[:invite_token])
          @resource.update(organization_id: @invite.organization_id)
          @invite.update(approved: true)
        end
      end
    end

  end
end