class UserDashboardController < ApplicationController
  before_action :configure_permitted_parameters, if: :devise_controller?


  protected

  def configure_permitted_parameters
    @parameters = [:first_name, :last_name]
    devise_parameter_sanitizer.for(:sign_up) << @parameters
    devise_parameter_sanitizer.for(:account_update) << @parameters
  end

end