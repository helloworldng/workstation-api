class UserDashboard::V1::FeedController < UserDashboardController
  before_action :authenticate_user!

  def key
    render json: {key: ENV["STREAM_KEY"]}, status: 200
  end

end