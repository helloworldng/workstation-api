require "base64"

class UserDashboard::V1::UsersController < UserDashboardController
  before_action :authenticate_user!


  def add_primary_location
    param! :location_id, Integer, required
    if Location.exists?(params[:location_id])
      @location = Location.find(params[:location_id])
      current_user.update(home_location_id: @location.id)
      render json: current_user, status: 201
    else
      render json: {errors: 'Location does not exist'}, status: 404
    end
  end

  # def subscribe_to_plan
  #   param! :plan_id, String, required: true
  #   @plan = Plan.find.friendly(params[:plan_id])
  #   current_user.update(plan_id: @plan.id)
  #   render json: current_user, status: 201
  # end


  def set_paystack_authorization
    param! :authorization, String, required: true
    auth = Base64.decode(params[:authorization])
    current_user.update(paystack_authorization: auth)
    head status: 204
  end

  def get_paystack_authorization
    auth = Base64.encode(current_user.paystack_authorization)
    render json: {authorization: auth}
  end


  private

end