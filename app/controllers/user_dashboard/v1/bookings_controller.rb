class UserDashboard::V1::BookingsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_meeting_room

  def index
    @bookings = Booking.where("meeting_room_id = ? AND end_time >= ?", @meeting_room.id, Time.now).order(:start_time)
    paginate json: @bookings, status: 201
  end


  def create
    validate_create_params
    @booking = Booking.new(booking_params)
    @booking.meeting_room = @meeting_room
    @booking.user = current_user
    if @booking.save
      render json: @booking, status: 201
    else
      render json: {errors: @booking.full_messages}, status: 422
    end
  end

  def destroy
    set_booking
    @booking.destroy
    head status: 204
  end

  def update
    set_booking
    if @booking.update(booking_params)
      render @booking, status: 201
    else
      render json: {errors: @booking.full_messages}, status: 422
    end
  end

  private


  def find_meeting_room
    if params[:meeting_room_id]
      @meeting_room = MeetingRoom.find_by_id(params[:meeting_room_id])
    end
  end

  def svalidate_create_params
    param! :meeting_room_id, Integer, required: true
    param! :start_time, DateTime, required: true
    param! :length, Integer, required: true
  end

  def booking_params
    params.permit(:meeting_room_id, :start_time, :length)
  end

  def set_booking
    param! :id, Integer, required: true
    @booking = Booking.find(params[:id])
  end

end
