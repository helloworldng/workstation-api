class UserDashboard::V1::OrganizationsController < UserDashboardController
  before_action :authenticate_user!

  def create
    ActiveRecord::Base.transaction do
      param! :name, String, required: true
      if !current_user.organization.exists?
        @organization = Organization.create(name: params[:name], admin_id: current_user.id)
        current_user.update(organization_id: @organization.id)
        #TODO change method of choosing organization default/inital plan to something using subscriptions
        @organization.update(plan_id: current_user.plan_id)
        render json: @organization, status: 201
      else
        render json: {errors: 'User already has organization'}, status: 400
      end
    end
  end

  def invite
    param! :email, String, required: true
    param! :invites_url, String, required: true
    param! :create_account_url, String, required: true
    if current_user_is_organization_admin?
      process_invitation
    end
  end

  def invites
    @invites = OrganizationInvite.where(recipient_id: current_user.id)
    paginate json: @invites, status: 200
  end

  def accept_invite
    param! :invite_id, Integer, required: true
    ActiveRecord::Base.transaction do
      @invite = OrganizationInvite.find(params[:invite_id])
      current_user.update(organization_id: @invite.organization_id)
      @invite.update(approved: true)
    end
  end

  def update
    param! :name, String, required: true
    set_organization
    if @organization.update(params[:name])
      render json: @organization, status: 201
    else
      render json: {errors: @organization.full_messages}, status: 422
    end
  end


  def set_paystack_authorization
    param! :authorization, String, required: true
    set_organization
    auth = Base64.decode(params[:authorization])
    @organization.update(paystack_authorization: auth)
    head status: 204
  end

  def get_paystack_authorization
    set_organization
    auth = Base64.encode(@organization.paystack_authorization)
    render json: {authorization: auth}
  end

  def destroy
    set_organization
    @organization.destroy
    head status: 204
  end

  private

  def set_organization
    @organization = current_user.organization
  end

  def invite_params
    params.permit(:sender)
  end

  def current_user_is_organization_admin?
    (!current_user.organization.blank?) && (current_user.organization.admin_id == current_user.id)
  end

  def process_invitation
    create_organization_invite
    if recipient_exists?
      set_recipient
      set_invite_recipient
      send_invite
    else
      send_invite_to_inexistent_user
    end
  end


  def recipient_exists?
    User.exists?(email: params[:email])
  end

  def set_recipient
    @recipient = User.find_by_email(params[:email])
  end

  def create_organization_invite
    @organization_invite = OrganizationInvite.new
    @organization_invite.sender = current_user.id
    @organization_invite.organization_id = current_user.organization.id
    @organization_invite.save
  end

  def set_invite_recipient
    @organization_invite.update(recipient_id: @recipient.id)
  end

  def send_invite
    InviteMailer.create_existing_user_invite(@organization_invite, params[:invites_url]).deliver
  end

  def send_invite_to_inexistent_user
    InviteMailer.create_inexistent_user_invite(@organization_invite, params[:create_account_url]).deliver
  end
end