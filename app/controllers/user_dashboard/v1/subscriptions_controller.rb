class UserDashboard::V1::SubscriptionsController < UserDashboardController
  before_action :authenticate_user!

  def subscribe
    validate_subscription_params
    @subscription = Subscription.create(subscription_params)
  end


  private


  def validate_subscription_params
    param! :subscriber_id, required: true
    param! :subscriber_type, String, transform: :capitalize
    param! :interval, String, required: true
  end

  def subscription_params
    params.permit(:subscriber_id, :subscriber_type, :interval)
  end


end