class UserDashboard::V1::MessagingController < UserDashboardController
  before_action :authenticate_user!

  def send
    verify_send_params
    set_recipient
    current_user.send_message(@recipient, params[:message], params[:subject])
    head status: 204
  end

  def conversations
    @conversations = current_user.mailbox.conversations
    paginate @conversations, status: 200
  end

  def reply_to_conversation
    param! :conversation_id, Integer, required: true
    param! :message, String, required: true
    @conversation = Mailboxer::Conversation.find(params[:conversation_id])
    current_user.reply_to_conversation(@conversation, params[:message])
    head status: 204
  end

  def receipts
    param! :conversation_id
    @conversation = Mailboxer::Conversation.find(params[:conversation_id])
    @receipts = @conversation.receipts_for current_user
    paginate @receipts, each_serializer: ReceiptSerializer, status: 200
  end

  def mark_as_read
    set_receipt
    @receipt.mark_as_read
    head status: 204
  end

  def mark_as_unread
    set_receipt
    @receipt.mark_as_unread
    head status: 204
  end

  def inbox
    @inbox = current_user.mailbox.inbox
    paginate @inbox, status: 200
  end

  def sent
    @sent = current_user.mailbox.sentbox
    paginate @sent, status: 200
  end

  private

  def verify_send_params
    param! :message, String, required: true
    param! :subject, String, required: true
    param! :recipient_id, Integer, required: true
    param! :recipient_type, String, required: true
  end

  def set_recipient
    if !params[:recipient_type] || params[:recipient_type].capitalize == "User"
      @recipient = User.find(params[:recipient_id])
    elsif params[:recipient_type].capitalize == "Admin"
      @recipient = Admin.find(params[:recipient_id])
    end
  end

  def set_receipt
    param! :receipt_id, Integer, required: true
    @receipt = Mailboxer::Receipt.find(params[:receipt_id])
  end
end