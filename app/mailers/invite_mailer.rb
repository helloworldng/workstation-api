class InviteMailer < ApplicationMailer

  def existing_user_invite(organization_invite, redirect_url)
    @sender = organization_invite.sender
    @recipient = organization_invite.recipient
    @redirect_url = redirect_url
    mail(to: @recipient.email, subject: "You have been invited to join #{@sender.organization}")
  end

  def inexistent_user_invite(organization_invite, redirect_url)
    @sender = organization_invite.sender
    @recipient = organization_invite.recipient
    @redirect_url = redirect_url + "?token=#{organization_invite.token}"
    mail(to: @recipient.email, subject: "You have been invited to #{@sender.organization} on Workstation")
  end

end