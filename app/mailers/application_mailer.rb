class ApplicationMailer < ActionMailer::Base
  default from: 'info@workstationng.com'
  layout 'mailer'

end
