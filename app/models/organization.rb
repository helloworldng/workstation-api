class Organization < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :admin, :class_name => "User"
  has_many :users
  has_many :organization_invites
  has_one :subscription, as: :subscriber, dependent: :destroy

  before_create :create_paystack_customer
  before_update :update_paystack_customer
  before_destroy :remove_all_users


  def create_paystack_customer
    result = $customers.create(
        first_name: self.name,
        email: self.admin.email
    )
    self.update(paystack_customer_code: result["data"]["customer_code"])
  end

  def update_paystack_customer
    result = $customers.update(
        self.paystack_customer_code,
        first_name: self.name,
        email: self.admin.email
    )
  end

  def remove_all_users
    self.users.each { |user| user.update(organization_id: nil) }
  end


end
