class Location < ApplicationRecord
  has_many :users
  has_many :admins
  has_many :meeting_rooms



  def country
    @country = Country.find_country_by_country_code(self.country_code)
    return {
        name: @country.data["name"],
        code: @country.data["country_code"]
    }
  end

  def state
    @country = Country.find_country_by_country_code(self.country_code)
    @state = @country.states.select{|key, value| key == self.state_shortcode }
    return {
        name: @state[self.state_shortcode]["name"],
        shortcode: self.state_shortcode
    }
  end


end
