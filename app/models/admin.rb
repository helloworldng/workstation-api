class Admin < ApplicationRecord
  acts_as_messageable
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  belongs_to :home_location, class_name: "Location"


end
