require_relative './concerns/bookable'

class Booking < ApplicationRecord
  include Bookable
  belongs_to :user

end