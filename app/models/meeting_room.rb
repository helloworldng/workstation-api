class MeetingRoom < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :bookings
  belongs_to :location
end

