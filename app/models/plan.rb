require 'paystack'

class Plan < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :users
  has_many :subscriptions

  before_create :create_paystack_plans
  before_update :update_paystack_plans


  def create_paystack_plans
    create_plan(:daily, self.daily_price)
    create_plan(:monthly, self.monthly_price)
    create_plan(:yearly, self.yearly_price)
  end


  def update_paystack_plans
    update_plan(:daily, self.daily_price)
    update_plan(:monthly, self.monthly_price)
    update_plan(:yearly, self.yearly_price)
  end


  private

  def create_plan(interval, price)
    result = $plans.create(
        name: self.name,
        description: self.descripion,
        interval: interval.to_s,
        currency: "NGN",
        amount: price * 100 # to kobo
    )
    plan_code = result["data"]["plan_code"]
    interval_code_string = interval.to_s + "_code"
    self.update(interval_code_string.to_sym => plan_code)
  end


  def update_plan(interval, price)
    interval_code_string = interval.to_s + "_code"
    result = $plans.update(
        self.send(interval_code_string),
        name: self.name,
        description: self.descripion,
        interval: interval.to_s,
        currency: "NGN",
        amount: price * 100 # to kobo
    )
  end

end
