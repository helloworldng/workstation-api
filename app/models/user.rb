class User < ApplicationRecord
  acts_as_messageable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  include DeviseTokenAuth::Concerns::User

  belongs_to :home_location, class_name: "Location"
  belongs_to :plan
  belongs_to :organization
  has_many :invitations, class_name: "OrganizationInvite", foreign_key: 'recipient_id'
  has_many :sent_invites, class_name: "Organization", foreign_key: 'sender_id'
  has_many :bookings
  has_one :subscription, as: :subscriber, dependent: :destroy

  before_create :create_paystack_customer
  before_update :update_paystack_customer


  def create_paystack_customer
    result = $customers.create(
        first_name: self.first_name,
        last_name: self.last_name,
        email: self.email
    )
    self.update(paystack_customer_code: result["data"]["customer_code"])
  end

  def update_paystack_customer
    result = $customers.update(self.paystack_customer_code,
                               first_name: self.first_name,
                               last_name: self.last_name,
                               email: self.email)
  end

  def name
    "#{first_name} #{last_name}"
  end


end