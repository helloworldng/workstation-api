class CreateOrganizationInvites < ActiveRecord::Migration
  def change
    create_table :organization_invites do |t|
      t.integer :sender_id
      t.integer :recipient_id
      t.integer :organization_id
      t.string :token
      t.boolean :approved

      t.timestamps null: false
    end
  end
end
