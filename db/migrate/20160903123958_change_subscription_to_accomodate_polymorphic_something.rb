class ChangeSubscriptionToAccomodatePolymorphicSomething < ActiveRecord::Migration[5.0]
  def change
    rename_column :subscriptions, :user_id, :subscriber_id
    add_column :subscriptions, :subscriber_type, :string
  end
end
