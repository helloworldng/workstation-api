class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.string :slug
      t.string :description
      t.decimal :monthly_price
      t.integer :tokens

      t.timestamps null: false
    end
    add_index :plans, :slug, unique: true
  end
end
