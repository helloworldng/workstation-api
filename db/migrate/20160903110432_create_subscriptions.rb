class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
      create_table :subscriptions do |t|
        t.integer :user_id
        t.integer :plan_id
        t.string :subscription_code
        t.string :interval

      t.timestamps
    end
  end
end
