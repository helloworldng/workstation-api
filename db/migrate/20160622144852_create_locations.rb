class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :street_address
      t.integer :country_id
      t.integer :state_id

      t.timestamps null: false
    end
  end
end
