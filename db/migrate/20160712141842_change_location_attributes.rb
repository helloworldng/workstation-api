class ChangeLocationAttributes < ActiveRecord::Migration
  def change
    rename_column :locations, :country_id, :country_code
    change_column :locations, :state_id, :string
    rename_column :locations, :state_id, :state_shortcode
  end
end
