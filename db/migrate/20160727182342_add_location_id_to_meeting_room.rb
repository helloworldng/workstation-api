class AddLocationIdToMeetingRoom < ActiveRecord::Migration
  def change
    add_column :meeting_rooms, :location_id, :integer
  end
end
