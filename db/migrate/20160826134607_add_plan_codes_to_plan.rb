class AddPlanCodesToPlan < ActiveRecord::Migration[5.0]
  def change
    add_column :plans, :daily_code, :string
    add_column :plans, :monthly_code, :string
    add_column :plans, :yearly_code, :string
  end
end
