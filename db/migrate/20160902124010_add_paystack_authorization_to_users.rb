class AddPaystackAuthorizationToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :paystack_authorization, :string
  end
end
