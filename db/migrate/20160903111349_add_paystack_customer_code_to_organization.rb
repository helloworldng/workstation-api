class AddPaystackCustomerCodeToOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :paystack_customer_code, :string
  end
end
