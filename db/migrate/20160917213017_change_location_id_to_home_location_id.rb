class ChangeLocationIdToHomeLocationId < ActiveRecord::Migration[5.0]
  def change
    rename_column :admins, :location_id, :home_location_id
  end
end
