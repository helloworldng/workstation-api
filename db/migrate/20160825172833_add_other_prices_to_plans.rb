class AddOtherPricesToPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :plans, :yearly_price, :decimal
    add_column :plans, :daily_price, :decimal
  end
end
