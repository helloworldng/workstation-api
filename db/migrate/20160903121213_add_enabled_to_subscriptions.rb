class AddEnabledToSubscriptions < ActiveRecord::Migration[5.0]
  def change
    add_column :subscriptions, :enabled, :boolean, default: true
    add_column :subscriptions, :email_token, :string
  end
end
