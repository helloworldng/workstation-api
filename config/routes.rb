Rails.application.routes.draw do


  mount_devise_token_auth_for 'User', at: '/user_dashboard/auth',
                              controllers: {
                                  registrations: 'overrides/registrations',
                                  sessions: 'overrides/sessions',
                                  token_validations: 'overrides/token_validations'
                              }

  namespace :user_dashboard, defaults: {format: :json} do
    scope module: :v1 do
      resources :users, only: [] do
        collection do
          post 'add_home_location'
          # post 'subscribe_to_plan'
          post 'authorization', to: 'users#set_paystack_authorization'
          get 'authorization', to: 'users#get_paystack_authorization'
        end
      end
      resources :organizations, except: [:new, :edit, :index, :show] do
        collection do
          post 'invite'
          get 'invites'
          post 'authorization', to: 'organizations#set_paystack_authorization'
          get 'authorization', to: 'organizations#get_paystack_authorization'
        end
      end
      get 'key', to: 'feed#key'
      resources :messaging, only: [] do
        collection do
          post 'send'
          get 'conversations'
          post 'reply_to_conversation/:conversation_id', to: 'messaging#reply_to_conversation'
          get 'receipts/:conversation_id', to: 'messaging#receipts'
          post 'mark_as_read/:receipt_id', to: 'messaging#mark_as_read'
          post 'unmark_as_read/:receipt_id', to: 'messaging#unmark_as_read'
          get 'inbox'
          get 'sent'
        end
      end
      resources :bookings
    end
  end


  mount_devise_token_auth_for 'Admin', at: '/admin_dashboard/auth',
                              controllers: {
                                  registrations: 'overrides/registrations',
                                  sessions: 'overrides/sessions',
                                  token_validations: 'overrides/token_validations',
                                  passwords: 'overrides/passwords'
                              }

  namespace :admin_dashboard, defaults: {format: :json} do
    scope module: :v1 do
      resources :plans, except: [:new, :edit] do
        member do
          get 'subscribers'
        end
      end
      resources :locations, except: [:new, :edit] do
        get 'users'
      end
      resources :countries, only: [:index, :show]
      put 'update_state/:id', to: 'countries#update_state'
      delete 'destroy_state/:id', to: 'countries#destroy_state'
      resources :meeting_rooms
      get 'admins', to: 'admins#admins'
    end
  end

end



